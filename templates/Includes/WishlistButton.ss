<% if $Top.WishList.Products.byID($Product.ID) %>
    <a href="#" title="Remove $Product.Title to your wish list" data-id="$Product.ID" class="btn-product btn-wishlist wishlist-toggle">
        <span class="ion-ios-heart"></span>
        Remove From Wishlist
    </a>
<% else %>
    <a href="#" title="Add $Product.Title to your wish list" data-id="$Product.ID" class="btn-product btn-wishlist wishlist-toggle">
        <span class="ion-ios-heart-outline"></span>
        Add To Wishlist
    </a>
<% end_if %>
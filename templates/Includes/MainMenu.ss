<nav>
    <ul>
        <% loop $SiteSettings.MainMenu.MenuItems %>
            <% if $Children.count %>
            <li><a href="#">$Title<i class="fa fa-angle-down"></i></a>
                <ul class="sub_menu">
                    <% loop $Children %>
                        <li><a href="$Link">$Title</a></li>
                    <% end_loop %>
                </ul>
            </li>
            <% else %>
                <li <% if isCurrent %>class="active"<% end_if %>>
                    <a href="$Link">$Title</a>
                </li>
            <% end_if %>
        <% end_loop %>
    </ul>
</nav>
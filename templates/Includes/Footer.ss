<footer class="footer_widgets footer_black">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="widgets_container contact_us">
                        <img class="footer_img" src="$SiteSettings.Logo.AbsoluteLink" alt="$SiteSettings.Title Logo"/>
                        <div class="footer_contact">
                            <% if $ContactPage.Address %>
                                <p>Address: $ContactPage.Address</p>
                            <% end_if %>
                            <% if $ContactPage.Tel %>
                                <p>Phone: <a href="tel:$ContactPage.Tel">$ContactPage.Tel</a></p>
                            <% end_if %>
                            <% if $ContactPage.Email %>
                                <p>Email: <a href="mailto:$ContactPage.Email">$ContactPage.Email</a></p>
                            <% end_if %>
                            <% if $SocialLinks.count %>
                                <ul>
                                    <% loop $SocialLinks %>
                                        <li><a href="$URL" target="_blank"><i class="ion-social-$Site.LowerCase"> </i></a></li>
                                    <% end_loop %>
                                </ul>
                            <% end_if %>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-4 col-6">
                    <div class="widgets_container widget_menu">
                        <h3>$Theme.CustomField('FooterMenu1').Value().Title</h3>
                        <div class="footer_menu">
                            <ul>
                                <% loop $Theme.CustomField('FooterMenu1').Value().MenuItems %>
                                    <li>
                                        <a href="$Link">$Title</a>
                                    </li>
                                <% end_loop %>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-5 col-6">
                    <div class="widgets_container widget_menu">
                        <h3>$Theme.CustomField('FooterMenu2').Value().Title</h3>
                        <div class="footer_menu">
                            <ul>
                                <% loop $Theme.CustomField('FooterMenu2').Value().MenuItems %>
                                    <li>
                                        <a href="$Link">$Title</a>
                                    </li>
                                <% end_loop %>
                            </ul>
                        </div>
                    </div>
                </div>
                <% if $Theme.CustomField('FooterWidget').Value() %>
                    <div class="col-lg-4 col-md-6 col-sm-7">
                    <div class="widgets_container product_widget">
                        $Theme.CustomField('FooterWidget')
                    </div>
                </div>
                <% end_if %>
            </div>
        </div>
        <hr/>
        <div class="footer_bottom py-2">
            <div class="row">
                <div class="col-12">
                    <div class="copyright_area">
                        <p>&copy; $Now.Format('Y')  <a href="/" class="text-uppercase">$SiteSettings.Title</a>. Made with <i class="fa fa-heart"></i> by <a target="_blank" href="https://lifeboat.app">Lifeboat</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
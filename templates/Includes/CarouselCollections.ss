<section class="product_section p_section1 product_black_section bottom_two pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="product_area">
                    <div class="product_tab_button">
                        <ul class="nav" role="tablist">
                            <% if $Theme.CustomField('TabbedCarousel1').Value() %>
                                <li>
                                    <a data-toggle="tab" href="#tabs-{$Theme.CustomField('TabbedCarousel1').Value().ID}" role="tab">
                                        $Theme.CustomField('TabbedCarousel1').Value().Title
                                   </a>
                                </li>
                            <% end_if %>
                            <% if $Theme.CustomField('TabbedCarousel2').Value() %>
                                <li>
                                    <a data-toggle="tab" href="#tabs-{$Theme.CustomField('TabbedCarousel2').Value().ID}" role="tab">
                                        $Theme.CustomField('TabbedCarousel2').Value().Title
                                    </a>
                                </li>
                            <% end_if %>
                            <% if $Theme.CustomField('TabbedCarousel3').Value() %>
                                <li>
                                    <a data-toggle="tab" href="#tabs-{$Theme.CustomField('TabbedCarousel3').Value().ID}" role="tab">
                                        $Theme.CustomField('TabbedCarousel3').Value().Title
                                    </a>
                                </li>
                            <% end_if %>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <% if $Theme.CustomField('TabbedCarousel1').Value() %>
                            <div class="tab-pane fade show active" id="tabs-{$Theme.CustomField('TabbedCarousel1').Value().ID}" role="tabpanel">
                                <div class="product_container">
                                    <div class="custom-row product_column3">
                                        <% loop $Theme.CustomField('TabbedCarousel1').Value().Products.limit(10) %>
                                            <% include ProductCardHome Product=$Me %>
                                        <% end_loop %>
                                    </div>
                                </div>
                            </div>
                        <% end_if %>
                        <% if $Theme.CustomField('TabbedCarousel2').Value() %>
                            <div class="tab-pane fade" id="tabs-{$Theme.CustomField('TabbedCarousel2').Value().ID}" role="tabpanel">
                                <div class="product_container">
                                    <div class="custom-row product_column3">
                                        <% loop $Theme.CustomField('TabbedCarousel2').Value().Products.limit(10) %>
                                            <% include ProductCardHome Product=$Me %>
                                        <% end_loop %>
                                    </div>
                                </div>
                            </div>
                        <% end_if %>
                        <% if $Theme.CustomField('TabbedCarousel3').Value() %>
                            <div class="tab-pane fade" id="tabs-{$Theme.CustomField('TabbedCarousel3').Value().ID}" role="tabpanel">
                                <div class="product_container">
                                    <div class="custom-row product_column3">
                                        <% loop $Theme.CustomField('TabbedCarousel3').Value().Products.limit(10) %>
                                            <% include  ProductCardHome Product=$Me %>
                                        <% end_loop %>
                                    </div>
                                </div>
                            </div>
                        <% end_if %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li>></li>
                        <li>Wishlist</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wishlist_area">
    <div class="container">
        <% if $WishList.Products.count %>
            <div class="row">
                <div class="col-12">
                    <div class="table_desc wishlist">
                        <div class="cart_page table-responsive">
                            <table>
                                <thead>
                                <tr>
                                    <th class="product_remove">Delete</th>
                                    <th class="product_thumb">Image</th>
                                    <th class="product_name">Product</th>
                                    <th class="product-price">Price</th>
                                    <th class="product_quantity">Stock Status</th>
                                    <th class="product_total">Add To Cart</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <% loop $PaginatedResults(6) %>
                                        <% loop $WishList.Products %>
                                            <tr>
                                                <td class="product_remove">
                                                    <a href="#" title="Remove $Title to your wish list" data-id="$ID" class="btn-product-icon btn-wishlist wishlist-toggle">
                                                        X
                                                    </a>
                                                </td>
                                                <td class="product_thumb">
                                                    <a href="$AbsoluteLink">
                                                        <% if $Image %>
                                                            <img src="$Image.Pad(100,100).Link" alt="$Title"/>
                                                        <% else %>
                                                            <img src="$SiteSettings.Logo.Pad(100,100).Link" alt="$Title"/>
                                                        <% end_if %>
                                                    </a>
                                                </td>
                                                <td class="product_name"><a href="$AbsoluteLink">$Title</a></td>
                                                <% if $Variants.count %>
                                                    <% if $Product.MinPrice != $MaxPrice %>
                                                        <td class="current_price">$FormatPrice('MinPrice') - $FormatPrice('MaxPrice')</td>
                                                    <% else %>
                                                        <td class="current_price">$Product('MinPrice')</td>
                                                    <% end_if %>
                                                <% else %>
                                                    <% if $isDiscounted %>
                                                        <td><span class="old-price">$FormatPrice('BasePrice')</span><span class="current_price">$FormatPrice('SellingPrice')</span></td>
                                                    <% else %>
                                                        <td class="current_price"> $FormatPrice('BasePrice')</td>
                                                    <% end_if %>
                                                <% end_if %>
                                                <td class="product_quantity"><% if $hasStockLeft %> In <% else %> Out of <% end_if %>Stock</td>
                                                <% if $Variants.count || not $hasStockLeft %>
                                                    <td class="product_total"><a href="$AbsoluteLink">View Product</a></td>
                                                <% else %>
                                                    <td class="product_total add_to_cart"><a href="#" class="add-to-cart" data-role="add-to-cart" data-id="$ID">Add to Cart</a></td>
                                                <% end_if %>
                                            </tr>
                                        <% end_loop %>
                                    <% end_loop %>
                                </tbody>
                            </table>
                            <% if $PaginatedPages(6).MoreThanOnePage %>
                                <div class="blog_pagination">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="pagination">
                                                    <ul>
                                                        <% if $PaginatedPages(6).NotFirstPage %>
                                                            <li class="prev"><a href="$PaginatedPages(6).PrevLink">Prev</a></li>
                                                        <% end_if %>
                                                        <% loop $PaginatedPages(6).PaginationSummary %>
                                                            <% if $CurrentBool %>
                                                                <li class="current"><a href="$Link">$PageNum</a></li>
                                                            <% else %>
                                                                <% if $Link %>
                                                                    <li><a href="$Link">$PageNum</a></li>
                                                                <% else %>
                                                                    <li>...</li>
                                                                <% end_if %>
                                                            <% end_if %>
                                                        <% end_loop %>
                                                         <% if $PaginatedPages(6).NotLastPage %>
                                                            <li class="next"><a href="$PaginatedPages(6).NextLink">Next</a></li>
                                                        <% end_if %>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <% end_if %>
                        </div>
                    </div>
                </div>
            </div>
        <% else %>
            You have no products in your Wish list.
        <% end_if %>
    </div>
</div>